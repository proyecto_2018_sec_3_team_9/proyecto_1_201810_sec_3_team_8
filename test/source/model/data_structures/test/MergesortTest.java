package model.data_structures.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.IntegerComparator;
import model.data_structures.LinkedList;
import model.data_structures.MergeSort;

public class MergesortTest 
{

	private LinkedList<Integer> list;
	@Before
	public void setupEscenario1(){
		list = new LinkedList<Integer>();
		list.add(900);
		list.add(1);
		list.add(35);
		list.add(1500);
		list.add(520);
	}

	/**
	 * Test 1: Verifies method sort(list). <br>
	 * <b>Methods to prove:</b> <br>
	 * sort<br>
	 * By proving sort works, you're also proving all the other methods as it uses all of them (see implementation)<br>
	 */
	@Test
	public void testSort() {
		MergeSort.sort(list, new IntegerComparator(), true);
		list.listing();
		int pos = 0;
		while(list.hasNext()) {
			assertFalse("No está ordenada", list.get(pos).compareTo(list.get(pos+1)) >0);
			System.out.println(list.getCurrent());
			list.next();
		}
		System.out.println(list.getCurrent());
		assertEquals("La lista debe seguir teniendo la misma cantidad de elementos", 5, list.size());
	}
	
	/**
	 * Test 1: Verifies method sort(list) with descending order. <br>
	 * <b>Methods to prove:</b> <br>
	 * sort<br>
	 * By proving sort works, you're also proving all the other methods as it uses all of them (see implementation)<br>
	 */
	@Test
	public void testSort2() {
		MergeSort.sort(list, new IntegerComparator(), false);
		list.listing();
		int pos = 0;
		while(list.hasNext()) {
			assertFalse("No está ordenada", list.get(pos).compareTo(list.get(pos+1)) <0);
			System.out.println(list.getCurrent());
			list.next();
		}
		System.out.println(list.getCurrent());
		assertEquals("La lista debe seguir teniendo la misma cantidad de elementos", 5, list.size());
	}
}
