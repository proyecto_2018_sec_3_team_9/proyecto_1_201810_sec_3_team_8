package model.data_structures.test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.LinkedList;
import model.vo.Taxi;

public class LinkedListTest {

	private LinkedList<Taxi> list;
	private Taxi element1;
	private Taxi element2;
	private Taxi element3;
	private Taxi element4;
	private Taxi element5;
	/**
	 * Creates a new empty list. 
	 */
	public void setupEscenario1( )
	{
		list = new LinkedList<Taxi>();
	}
	public void setupEscenario2( )
	{
		list = new LinkedList<Taxi>();
		element1= new Taxi();
		element1.setId("123");
		element1.setCompany("Compañia del sabor");
		element2= new Taxi();
		element2.setId("1234");
		element2.setCompany("Compañia de taxis");
		element3= new Taxi();
		element3.setId("12345");
		element3.setCompany("Compañia cabify");
		element4= new Taxi();
		element4.setId("1236");
		element4.setCompany("Compañia tappsi");
		element5= new Taxi();
		element5.setId("1237");
		element5.setCompany("Compañia uber");
		list.add(element1);
		list.add(element2);
		list.add(element3);
		list.add(element4);
		list.add(element5);
	}

	/**
	 * Test 1: Verifies method add(). <br>
	 * <b>Methods to prove:</b> <br>
	 * add<br>
	 * get<br>
	 */
	@Test
	public void testAdd()
	{
		Taxi element= new Taxi();
		element.setId("123");
		element.setCompany("Compañia del sabor");
		setupEscenario1();
		list.add(element);
		assertNotNull("Se deberia agregar la banda",list.get(element));

	}

	/**
	 * Test 2: Verifies method delete(). <br>
	 * <b>Methods to prove:</b> <br>
	 * List<br>
	 * getCurrent<br>
	 * size<br>
	 * delete<br>
	 */
	@Test
	public void testRemove()
	{
		setupEscenario2();
		list.remove(list.getCurrent());
		assertEquals("El tamaño de la lista deberia ser 4", 4, list.size());
	}

	/**
	 * Test 3: Verifies method get(). <br>
	 * <b>Methods to prove:</b> <br>
	 * List<br>
	 * get<br>
	 */
	@Test 
	public void testGet()
	{
		Taxi element= new Taxi();
		element.setId("123");
		element.setCompany("Compañia del sabor");
		setupEscenario2();

		list.get(element);
		assertEquals("No es el elemento esperado",element2 ,list.get(element2));

	}

	/**
	 * Test 4: Verifies method size(). <br>
	 * <b>Methods to prove:</b> <br>
	 * List<br>
	 * size<br>
	 * <b> Cases: </b><br>
	 * 1) Size is 0. <br>
	 * 2) Size is 5.
	 */
	@Test
	public void testSize()
	{
		setupEscenario1( );
		assertTrue( "The size should be 0", list.size() == 0);

		setupEscenario2();
		assertTrue( "The size should be 5", list.size() == 5 );	
	}

	/**
	 * Test 5: Verifies method get(int). <br>
	 * <b>Methods to prove:</b> <br>
	 * List<br>
	 * get<br>
	 * add<br>
	 */
	@Test
	public void testGetByPos()
	{
		setupEscenario2();
		assertEquals("Is not the expected element",list.get(1) ,element2);

	}
	
	/**
	 * Test 6: Verifies method listing(). <br>
	 * <b>Methods to prove:</b> <br>
	 * List<br>
	 * listing<br>
	 * get<br>
	 * getCurrent<br>
	 */
	@Test
	public void testListing()
	{
		setupEscenario2();
		list.listing();
		assertEquals("Is not the expected element",element1, list.getCurrent());

	}
	
	/**
	 * Test 7: Verifies method getCurrent(). <br>
	 * <b>Methods to prove:</b> <br>
	 * List<br>
	 * next<br>
	 * get<br>
	 * getCurrent<br>
	 */
	@Test
	public void testGetCurrent()
	{
		setupEscenario2();
		list.next();
		list.next();
		assertEquals("Is not the expected element",element3, list.getCurrent() );

	}
	
	/**
	 * Test 8: Verifies method next(). <br>
	 * <b>Methods to prove:</b> <br>
	 * List<br>
	 * next<br>
	 * get<br>
	 * getCurrent<br>
	 */
	@Test
	public void testNext()
	{
		setupEscenario2();
		assertEquals("Is not the expected element",element2, list.next() );
	}
	
	/**
	 * Test 8: Verifies method hasNext(). <br>
	 * <b>Methods to prove:</b> <br>
	 * hasNext<br>
	 */
	@Test
	public void testHasNext()
	{
		setupEscenario1();
		assertFalse("There shouldn't be a next element", list.hasNext());
		setupEscenario2();
		assertTrue("There should be a next element",list.hasNext() );
	}
	
	/**
	 * Test 8: Verifies method set(element, index). <br>
	 * <b>Methods to prove:</b> <br>
	 * List<br>
	 * set<br>
	 * get<br>
	 */
	@Test
	public void testSet()
	{
		setupEscenario2();
		Taxi newTaxi = new Taxi();
		newTaxi.setId("214324");
		newTaxi.setCompany("");
		list.set(newTaxi, 3);
		assertEquals("Is not the expected element",	newTaxi, list.get(3));
	}

}
