package view;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


import controller.Controller;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Node;
import model.data_structures.ILinkedList;
import model.data_structures.LinkedList;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServicioResumen;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			//1C cargar informacion dada
			case 1:

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
					break;

					//direccion json grande
				case 3:

					linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
					break;
				}

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.cargarSistema(linkJson);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

				//1A	
			case 2:

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq1A = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq1A = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq1A = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq1A = sc.next();

				//VORangoFechaHora
				RangoFechaHora rangoReq1A = new RangoFechaHora(fechaInicialReq1A, fechaFinalReq1A, horaInicialReq1A, horaFinalReq1A);

				//Se obtiene la queue dada el rango
				IQueue<Servicio> colaReq1A = Controller.darServiciosEnRango(rangoReq1A);
				//TODO 
				//Recorra la cola y muestre cada servicio en ella
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

				while(!colaReq1A.isEmpty()) {
					Servicio servicio = colaReq1A.dequeue();
					System.out.println("Trip id:" + " " + servicio.getTripId() + " " + "Start time:" + " " + format.format(servicio.getStartTime()) + " " + "End time:" + " " + format.format(servicio.getEndTime()));	
				}

				break;

			case 3: //2A

				//comany
				sc.nextLine();
				System.out.println("Ingrese el nombre de la compa��a");
				String companyReq2A = sc.nextLine();

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq2A = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq2A = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq2A = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq2A = sc.next();

				//VORangoFechaHora
				RangoFechaHora rangoReq2A = new RangoFechaHora(fechaInicialReq2A, fechaFinalReq2A, horaInicialReq2A, horaFinalReq2A);
				Taxi taxiReq2A = Controller.darTaxiConMasServiciosEnCompaniaYRango(rangoReq2A, companyReq2A);

				//TODO
				//Muestre la info del taxi

				if(taxiReq2A != null)
				{
					System.out.println("Taxi id:" + " " + taxiReq2A.getTaxiId() + " " + "Compañia:" + taxiReq2A.getCompany());
				}
				else
				{
					System.out.println("La compañia no existe o no se encontraron taxis.");
				}
				break;


			case 4: //3A

				//comany
				System.out.println("Ingrese el id del taxi");
				String idTaxiReq3A = sc.next();

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq3A = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq3A = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq3A = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq3A = sc.next();

				//VORangoFechaHora
				RangoFechaHora rangoReq3A = new RangoFechaHora(fechaInicialReq3A, fechaFinalReq3A, horaInicialReq3A, horaFinalReq3A);
				InfoTaxiRango taxiReq3A = Controller.darInformacionTaxiEnRango(idTaxiReq3A, rangoReq3A);

				//TODO
				//Muestre la info del taxi

				if(taxiReq3A != null)
				{
					System.out.println("Compañia:" + " " + taxiReq3A.getCompany());
					System.out.println("Plata Ganada:" + " " + taxiReq3A.getPlataGanada());
					System.out.println("Servicios prestados:" + " " + taxiReq3A.getServiciosPrestadosEnRango().size());
					System.out.println("Distancia recorrida:" + " " + taxiReq3A.getDistanciaTotalRecorrida() );
					System.out.println("Tiempo total:" + " " + taxiReq3A.getTiempoTotal());
				}
				else
				{
					System.out.println("El taxi buscado no se encuentra o no existe la compañia");
				}
				break;

			case 5: //4A

				//fecha 
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaReq4A = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq4A = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq4A = sc.next();

				ILinkedList<RangoDistancia> listaReq4A = Controller.darListaRangosDistancia(fechaReq4A, horaInicialReq4A, horaFinalReq4A);

				//TODO
				//Recorra la lista y por cada VORangoDistancia muestre los servicios
				for(int i = 0; i < listaReq4A.size(); i++)
				{
					RangoDistancia actual = listaReq4A.get(i);
					LinkedList<Servicio> servicios= actual.getServiciosEnRango();
					System.out.println("Inferior:" + actual.getLimineInferior() + "millas");
					System.out.println("Superior:" + actual.getLimiteSuperior() + "millas");	
					
					for(int j = 0; j < servicios.size(); j++)
					{
						Servicio service= servicios.get(j);
						System.out.println("Trip id:" + service.getTripId());
					}
				}
				break;

			case 6: //1B
				ILinkedList<Compania> lista=Controller.darCompaniasTaxisInscritos();
				//TODO
				//Mostrar la informacion de acuerdo al enunciado
				Node<Compania> actual1 = lista.getNode(0);
				int totalComp = 0;
				int totalTax = 0;

				while(actual1!=null) {
					Compania actual = actual1.getElement();
					System.out.println(actual.getNombre() + " = " + actual.getTaxisInscritos().size());
					totalComp++;
					totalTax += actual.getTaxisInscritos().size();
					actual1 = actual1.getNext();
				}
				System.out.println("El total de compañías es: "+totalComp);
				System.out.println("El total de taxis inscritos es: " + totalTax);
				break;

			case 7: //2B
				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq2B = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq2B = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq2B = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq2B = sc.next();

				//Compania
				System.out.println("Ingrese el nombre de la compa�ia");
				sc.nextLine();
				String compania2B=sc.nextLine();

				//VORangoFechaHora
				RangoFechaHora rangoReq2B = new RangoFechaHora(fechaInicialReq2B, fechaFinalReq2B, horaInicialReq2B, horaFinalReq2B);

				Taxi taxi=Controller.darTaxiMayorFacturacion(rangoReq2B, compania2B);

				//TODO
				//Mostrar la informacion del taxi obtenido
				if(taxi!=null) {
					System.out.println("El id del taxi con mayor facturación en este periodo de la compañía " + compania2B + " es: " + taxi.getTaxiId());
				}
				else {
					System.out.println("La compañía no existe o no tiene taxis registrados");
				}

				break;

			case 8: //3B
				//Compania
				System.out.println("Ingrese el id de la zona");
				String zona3B=sc.next();

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq3B = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq3B = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq3B = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq3B = sc.next();

				RangoFechaHora rango3B= new RangoFechaHora(fechaInicialReq3B, fechaFinalReq3B, horaInicialReq3B, horaFinalReq3B);

				ServiciosValorPagado[] resp=Controller.darServiciosZonaValorTotal(rango3B, zona3B);
				String[] cases = new String[3];
				cases[0] = "recogieron en la zona buscada y terminaron en otra zona es de: ";
				cases[1] = "recogieron en otra zona y terminaron en la zona buscada es de: ";
				cases[2] = "recogieron en la zona buscada y terminaron en la zona buscada es de: ";
				//TODO
				//Mostrar la informacion de acuerdo al enunciado
				for(int i = 0; i<resp.length; i++) {
					ServiciosValorPagado actual = resp[i];
					System.out.println("El número total de servicios que se " + cases[i] + actual.getServiciosAsociados().size());
					System.out.println("Total pagado por los usuarios: $" + actual.getValorAcumulado());
				}
				break;

			case 9: //4B
				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq4B = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq4B = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq4B = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq4B = sc.next();

				RangoFechaHora rango4B= new RangoFechaHora(fechaInicialReq4B, fechaFinalReq4B, horaInicialReq4B, horaFinalReq4B);

				ILinkedList<ZonaServicios> lista4B= Controller.darZonasServicios(rango4B);
				//TODO
				//Mostrar la informacion de acuerdo al enunciado
				Node<ZonaServicios> actual = lista4B.getNode(0);
				while(actual != null) {
					ZonaServicios actual2 = actual.getElement();
					System.out.println("Zona #" + actual2.getIdZona());
					ILinkedList<FechaServicios> fs = actual2.getFechasServicios();
					if(fs.size()==0) {
						System.out.println("La zona no tiene servicios en este rango de fechas.");
					}
					else {
						Node<FechaServicios> actual3 = fs.getNode(0);
						while(actual3 != null) {
							FechaServicios actual4 = actual3.getElement();
							System.out.println("El total de servicios en esta zona para la fecha " + actual4.getFecha() + " es: " + actual4.getNumServicios());
							actual3 =actual3.getNext();
						}
					}
					actual = actual.getNext();
				}

				break;

			case 10: //2C
				System.out.println("Ingrese el n�mero n de compa�ias");
				int n2C=sc.nextInt();

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq2C = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq2C = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq2C = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq2C = sc.next();

				RangoFechaHora rango2C= new RangoFechaHora(fechaInicialReq2C, fechaFinalReq2C, horaInicialReq2C, horaFinalReq2C);

				ILinkedList<CompaniaServicios> lista2C= Controller.companiasMasServicios(rango2C, n2C);
				//TODO
				//Mostrar la informacion de acuerdo al enunciado				
				System.out.println("Top " + n2C + " de compañías según cantidad de servicios: ");
				int top = n2C;
				if(lista2C.size()!= 0){
					if(lista2C.size() < n2C) {
						top = lista2C.size();
						System.out.println("No hay " + n2C + " compañías, por lo que se muestra el top " + top);
					}
					Node<CompaniaServicios> actual2 = lista2C.getNode(0);
					while(actual2 != null && top >0) {
						System.out.println(top +". " + actual2.getElement().getNomCompania() + ". Número de servicios: "+ actual2.getElement().getServicios().size());
						top--;
						actual2 = actual2.getNext();
					}
				}

				break;

			case 11: //3C
				ILinkedList<CompaniaTaxi> lista3C=Controller.taxisMasRentables();
				//TODO
				//Mostrar la informacion de acuerdo al enunciado		
				Node<CompaniaTaxi> actual3 = lista3C.getNode(0);
				while(actual3 != null) {
					System.out.println("Taxi más rentable de la compañía " + actual3.getElement().getNomCompania() + ": "+ actual3.getElement().getTaxi().getTaxiId());
					actual3 = actual3.getNext();
				}

				break;

			case 12: //4C

				//id taxi
				System.out.println("Ingrese el id del taxi");
				String idTaxi4C=sc.next();

				//fecha 
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaReq4C = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq4C = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq4C = sc.next();

				IStack <Servicio> resp4C=Controller.darServicioResumen(idTaxi4C, horaInicialReq4C, horaFinalReq4C, fechaReq4C);
				//TODO
				//Mostrar la informacion de acuerdo al enunciado
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				System.out.println("Los servicios resumenes con una distancia recorrida máxima de 20 millas son: ");
				boolean yaEntre = false;
				while(!resp4C.isEmpty()) {
					Servicio actual4 = resp4C.pop();
					Date fechaIni = actual4.getStartTime();
					Date fechaFin = actual4.getEndTime();
					if(!actual4.getTripId().contains("Servicio Resumen") && !yaEntre) {
						yaEntre = true;
						System.out.println("Los siguientes ya no son resumenes porque sus distancias sumadas no igualan o superan las 20 millas:");
					}
					System.out.println("Id: " + actual4.getTripId() + ".");
					System.out.println("\t Fechas (Inicio - Fin): " + (fechaIni != null ? format1.format(fechaIni) : "No hay fecha de inicio") + " - " + (fechaFin != null ? format1.format(fechaFin) : "No hay fecha final"));
					System.out.println("\t Distancia recorrida: " + actual4.getTripMiles());
					System.out.println("\t Duración: " + actual4.getTripSeconds());
					System.out.println("\t Valor total: $" + actual4.getTripTotal());
				}

				break;

			case 13: //salir
				fin=true;
				sc.close();
				break;

			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 1----------------------");
		System.out.println("Cargar data (1C):");
		System.out.println("1. Cargar toda la informacion dada una fuente de datos (small,medium, large).");

		System.out.println("\nParte A:\n");
		System.out.println("2.Obtenga una lista con todos los servicios de taxi ordenados cronologicamente por su fecha/hora inicial, \n"
				+ " que se prestaron en un periodo de tiempo dado por una fecha/hora inicial y una fecha/hora final de consulta. (1A) ");
		System.out.println("3.Dada una compania y un rango de fechas y horas, obtenga  el taxi que mas servicios inicio en dicho rango. (2A)");
		System.out.println("4.Consulte la compania, dinero total obtenido, servicios prestados, distancia recorrida y tiempo total de servicios de un taxi dados su id y un rango de fechas y horas. (3A)");
		System.out.println("5.Dada una fecha y un rango de horas, obtenga una lista de rangos de distancia en donde cada pocision contiene los servicios de taxis cuya distancia recorrida pertence al rango de distancia. (4A)\n");

		System.out.println("Parte B: \n");
		System.out.println("6. Obtenga el numero de companias que tienen al menos un taxi inscrito y el numero total de taxis que trabajan para al menos una compania. \n"
				+ "Luego, genera una lista (en orden alfabetico) de las companias a las que estan inscritos los servicios de taxi. Para cada una, indique su nombre y el numero de taxis que tiene registrados. (1B)");
		System.out.println("7. Dada una compania, una fecha/hora inicial y una fecha/hora final, buscar el taxi de la compania que mayor facturacion gener� en el tiempo dado. (2B)");
		System.out.println("8. Dada una zona de la ciudad, una fecha/hora inicial y una fecha/hora final, dar la siguiente informacion: (3B) \n"
				+ "   -Numero de servicios que iniciaron en la zona dada y terminaron en otra zona, junto con el valor total pagado por los usuarios. \n"
				+ "   -Numero de servicios que iniciaron en otra zona y terminaron en la zona dada, junto con el valor total pagado por los usuarios. \n"
				+ "   -Numero de servicios que iniciaron y terminaron en la zona dada, junto con el valor total pagado por los usuarios.");
		System.out.println("9. Dado un rango de fechas, obtener la lista de todas las zonas, ordenadas por su identificador. Para cada zona, dar la lista de fechas dentro del rango (ordenadas cronol�gicamente) \n "
				+ "y para cada fecha, dar el numero de servicios que se realizaron en dicha fecha. (4B)");


		System.out.println("\nParte C: \n");
		System.out.println("10. Dado un numero n, una fecha/hora inicial y una fecha/hora final, mostrar las n companias que mas servicios iniciaron dentro del rango. La lista debe estar ordenada descendentemente \n por el numero de servicios. Para cada compania, dar el nombre y el numero de servicios (2C)");
		System.out.println("11. Para cada compania, dar el taxi mas rentable. La rentabilidad es dada por la relacion entre el dinero ganado y la distancia recorrida. (3C)");
		System.out.println("12. Dado un taxi, dar el servicio resumen, resultado de haber comprimido su informacion segun el enunciado. (4C) ");
		System.out.println("13. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	}

}
