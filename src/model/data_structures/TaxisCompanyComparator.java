package model.data_structures;

import java.util.Comparator;

import model.vo.Taxi;

public class TaxisCompanyComparator implements Comparator<Taxi> {

	@Override
	public int compare(Taxi o1, Taxi o2) {
		// TODO Auto-generated method stub
		String company1 =o1.getCompany();
		String company2 =o2.getCompany();
		if(company1 != null && company2 != null) {
			int comparar = company1.compareTo(company2);

			if(comparar > 0)
			{
				return 1;
			}
			else if(comparar < 0)
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}
		else {
			if(company1 == null && company2!=null) {
				return 1;
			}
			else if(company2 == null && company1 != null) {
				return -1;
			}
			else {
				return 0;
			}
		}
	}

}
