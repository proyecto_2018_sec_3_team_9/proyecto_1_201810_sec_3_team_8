package model.data_structures;

import java.util.Comparator;

public class IntegerComparator implements Comparator<Integer>{

	@Override
	public int compare(Integer o1, Integer o2) {
		// TODO Auto-generated method stub
		int comparar = o1 - o2;

		if(comparar > 0)
		{
			return 1;
		}
		else if(comparar < 0)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}

}
