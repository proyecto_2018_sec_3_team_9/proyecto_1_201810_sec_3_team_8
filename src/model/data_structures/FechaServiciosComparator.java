package model.data_structures;

import java.util.Comparator;

import model.vo.FechaServicios;

public class FechaServiciosComparator implements Comparator<FechaServicios>{

	@Override
	public int compare(FechaServicios o1, FechaServicios o2) {
		// TODO Auto-generated method stub
		int comparar = o1.compareTo(o2);

		if(comparar > 0)
		{
			return 1;
		}
		else if(comparar < 0)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}

}
