package model.data_structures;

import java.util.Comparator;

import model.vo.CompaniaServicios;

public class CompanyServicesComparator implements Comparator<CompaniaServicios> {

	@Override
	public int compare(CompaniaServicios o1, CompaniaServicios o2) {
		// TODO Auto-generated method stub
		int comparar = o1.getServicios().size() - o2.getServicios().size();

		if(comparar > 0)
		{
			return 1;
		}
		else if(comparar < 0)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}

}
