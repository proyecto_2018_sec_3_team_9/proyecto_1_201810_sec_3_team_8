package model.data_structures;

import java.util.Comparator;

import model.vo.Compania;

public class CompanyNameComparator implements Comparator<Compania>{

	@Override
	public int compare(Compania o1, Compania o2) {
		// TODO Auto-generated method stub
		int comparar = o1.compareTo(o2);

		if(comparar > 0)
		{
			return 1;
		}
		else if(comparar < 0)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}

}
