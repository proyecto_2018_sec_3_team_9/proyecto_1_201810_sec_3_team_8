package model.data_structures;

import java.util.Comparator;

import model.vo.Servicio;

public class ServiceDateComparator implements Comparator<Servicio> {

	@Override
	public int compare(Servicio o1, Servicio o2) {
		// TODO Auto-generated method stub
		int comparar = o1.compareByDateTo(o2);

		if(comparar > 0)
		{
			return 1;
		}
		else if(comparar < 0)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}

}
