package model.data_structures;

import java.util.Comparator;

import model.vo.Servicio;

public class ServiceCompanyTaxiIdComparator implements Comparator<Servicio>{

	@Override
	public int compare(Servicio o1, Servicio o2) {
		// TODO Auto-generated method stub
		String company1 =o1.getCompany();
		String company2 =o2.getCompany();
		int comparar2 = o1.getTaxiId().compareTo(o2.getTaxiId());
		if(company1 != null && company2 != null) {
			int comparar = company1.compareTo(company2);

			if(comparar > 0)
			{
				return 1;
			}
			else if(comparar < 0)
			{
				return -1;
			}
			else
			{
				if(comparar2 >0) {
					return 1;
				}
				else if(comparar2 < 0) {
					return -1;
				}
				else {
					return 0;
				}
			}
		}
		else {
			if(company1 == null && company2!=null) {
				return 1;
			}
			else if(company2 == null && company1 != null) {
				return -1;
			}
			else {
				return 0;
			}
		}
	}

}
