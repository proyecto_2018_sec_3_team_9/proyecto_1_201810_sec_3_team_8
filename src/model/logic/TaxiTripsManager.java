package model.logic;

import java.io.File;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.IntegerComparator;
import model.data_structures.LinkedList;
import model.data_structures.MergeSort;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.ServiceAreaComparator;
import model.data_structures.ServiceCompanyComparator;
import model.data_structures.ServiceCompanyTaxiIdComparator;
import model.data_structures.ServiceDateComparator;
import model.data_structures.ServiceDistanceComparator;
import model.data_structures.ServiceTaxiIdComparator;
import model.data_structures.Stack;
import model.data_structures.TaxisCompanyComparator;
import model.data_structures.CompanyNameComparator;
import model.data_structures.CompanyServicesComparator;
import model.data_structures.FechaServiciosComparator;
import model.data_structures.ILinkedList;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large";


	private  LinkedList<Servicio> list1;

	private  LinkedList<Taxi> list2;


	@Override //1C
	public boolean cargarSistema(String direccionJson) 
	{
		boolean cargo = true;
		list1 = new LinkedList<Servicio>();

		list2 = new LinkedList<Taxi>();

		System.out.println("Cargar sistema con" + direccionJson);
		File arch = new File(direccionJson);


		try 
		{
			if(arch.isDirectory()) {
				File[] files = arch.listFiles();
				for(int i = 0; i<files.length; i++) {
					Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
					Servicio[] service = gson.fromJson(new FileReader(files[i].getAbsolutePath()), Servicio[].class);

					for(Servicio s : service) {
						list1.add(s);
						Taxi taxi1 = new Taxi();
						taxi1.setId(s.getTaxiId());
						taxi1.setCompany(s.getCompany());
						list2.add(taxi1);
					}
				}
			}
			else {
				Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
				Servicio[] service = gson.fromJson(new FileReader(direccionJson), Servicio[].class);

				for(Servicio s : service) {
					list1.add(s);
					Taxi taxi1 = new Taxi();
					taxi1.setId(s.getTaxiId());
					taxi1.setCompany(s.getCompany());
					list2.add(taxi1);
				}
			}
			System.out.println("el tamaño de la lista de servicios es:" + list1.size());
			System.out.println("el tamaño de la lista de taxis es:" + list2.size());
		} 
		catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
			cargo = false;
		}
		return cargo;
	}

	@Override //1A
	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango)
	{
		Queue<Servicio> queue = new Queue<Servicio>();
		try {
			LinkedList<Servicio> aux = new LinkedList<Servicio>();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date initial = format.parse(rango.getFechaInicial() + " " + rango.getHoraInicio());
			Date finals = format.parse(rango.getFechaFinal() + " " + rango.getHoraFinal());	

			for(int i = 0; i < list1.size(); i++)
			{
				Servicio actual= list1.get(i);
				Date fecha = actual.getStartTime();
				Date fecha2 = actual.getEndTime();
				if(fecha != null && fecha2 != null)
				{
					if(((initial.compareTo(fecha) == 0) || fecha.after(initial)) && (((finals.compareTo(fecha2) == 0) || fecha2.before(finals))))
					{
						aux.add(actual);
					}
				}

			}
			MergeSort.sort(aux, new ServiceDateComparator(), true);
			for(int i = 0; i < aux.size(); i++)
			{
				Servicio actual2 = aux.get(i);
				if(aux.size() != 0)
				{
					queue.enqueue(actual2);
				}

			}
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
		return queue;
	}

	@Override //2A
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company)
	{
		// TODO Auto-generated method stub
		Taxi taxi = null;
		try {
			LinkedList<Servicio> aux = new LinkedList<Servicio>();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date initial = format.parse(rango.getFechaInicial() + " " + rango.getHoraInicio());
			Date finals = format.parse(rango.getFechaFinal() + " " + rango.getHoraFinal());	

			for(int i = 0; i < list1.size(); i++)
			{
				Servicio actual= list1.get(i);
				String compania = actual.getCompany();
				Date fecha = actual.getStartTime();
				Date fecha2 = actual.getEndTime();
				if((fecha != null && ((initial.compareTo(fecha) == 0) || fecha.after(initial))) && (fecha2 != null && ((finals.compareTo(fecha2) == 0) || fecha2.before(finals))) && compania != null && compania.equals(company))
				{
					aux.add(actual);
				}
			}

			MergeSort.sort(aux, new ServiceTaxiIdComparator(), true);

			String actual = aux.get(0).getTaxiId();
			int totalTaxis = 0;
			int cantMayor = Integer.MIN_VALUE;
			String idMayor = "Ninguno";
			for(int i = 0; i < aux.size(); i++) {
				String idComparar = aux.get(i).getTaxiId();
				if(actual.equals(idComparar)) {
					totalTaxis++;
				}
				else {
					if(totalTaxis > cantMayor) {
						cantMayor = totalTaxis;
						idMayor = actual;
					}
					totalTaxis = 1;
					actual = idComparar;
				}
			}

			taxi = new Taxi();
			taxi.setCompany(company);
			taxi.setId(idMayor);
		}

		catch (ParseException e) {
			e.printStackTrace();
		}

		return taxi;
	}

	@Override //3A
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		InfoTaxiRango info = new InfoTaxiRango();
		try {
			LinkedList<Servicio> aux = new LinkedList<Servicio>();
			String company = "Ninguna";
			LinkedList<Servicio> serviciosP = new LinkedList<Servicio>();
			double plata = 0.0;
			double distancia = 0.0;
			double tiempo = 0;
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date initial = format.parse(rango.getFechaInicial() + " " + rango.getHoraInicio());
			Date finals = format.parse(rango.getFechaFinal() + " " + rango.getHoraFinal());	

			for(int i = 0; i < list1.size(); i++)
			{
				Servicio actual= list1.get(i);
				Date fecha = actual.getStartTime();
				Date fecha2 = actual.getEndTime();
				if((fecha != null && ((initial.compareTo(fecha) == 0) || fecha.after(initial))) && (fecha2 != null && ((finals.compareTo(fecha2) == 0) || fecha2.before(finals))))
				{
					aux.add(actual);
				}
			}

			for(int j=0; j <aux.size(); j++) 
			{
				Servicio actual = list1.get(j);
				if(actual.getTaxiId().equals(id)) 
				{
					serviciosP.add(actual);
					plata += actual.getTripTotal();
					distancia += actual.getTripMiles();
					tiempo = actual.getTripSeconds();
					company = actual.getCompany();
				}
			}

			info.setServiciosPrestadosEnRango(serviciosP);
			info.setPlataGanada(plata);
			info.setDistanciaTotalRecorrida(distancia);
			info.setTiempoTotal("" + tiempo);
			info.setCompany(company);

		}
		catch (ParseException e) {
			e.printStackTrace();
		}
		return info;
	}

	@Override //4A
	public ILinkedList<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{

		LinkedList<RangoDistancia> lista = new LinkedList();
		LinkedList<Servicio> services = new LinkedList<Servicio>();
		RangoFechaHora rango = new RangoFechaHora(fecha, fecha, horaInicial, horaFinal);
		double max = 0.0;
		MergeSort.sort(list1, new ServiceDistanceComparator(), true);
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date initial = format.parse(rango.getFechaInicial() + " " + rango.getHoraInicio());
			Date finals = format.parse(rango.getFechaFinal() + " " + rango.getHoraFinal());
			int contador = 0;
			String id = "Ninguno";

			while(contador < list1.size()) 
			{
				Servicio actual = list1.get(contador);
				Date start = actual.getStartTime();
				Date end = actual.getEndTime();
				if((fecha != null && ((initial.compareTo(start) == 0) || start.after(initial))) && (finals.compareTo(end) == 0) || end.before(finals))
				{
					services.add(actual);
					if(actual.getTripMiles() > max)
					{
						max = actual.getTripMiles();
						id = actual.getTripId();
					}
				}
				contador++;
			}
			for(double i = 0; i < max; i++)
			{
				LinkedList<Servicio> serviciosRango = new LinkedList<Servicio>();
				double limiteSup = i;
				double limiteInf = i+1;
				for(int j = 0; j < services.size(); j++)
				{
					Servicio actual = services.get(j);
					if(actual.getTripMiles() >= limiteInf && actual.getTripMiles() < limiteSup)
					{
						serviciosRango.add(actual);
					}
				}				
				RangoDistancia rangoDistancia = new RangoDistancia();
				rangoDistancia.setLimineInferior(limiteInf);
				rangoDistancia.setLimiteSuperior(limiteSup);
				rangoDistancia.setServiciosEnRango(serviciosRango);
				lista.add(rangoDistancia);
			}
			//			System.out.println(id);
		}
		catch (ParseException e) 
		{
			e.printStackTrace();
		}

		return lista;
	}

	@Override //1B
	public ILinkedList<Compania> darCompaniasTaxisInscritos() 
	{
		// TODO Auto-generated method stub
		LinkedList<Compania> companias = new LinkedList<Compania>();
		MergeSort.sort(list2, new TaxisCompanyComparator(), true);
		if(list2.size()!=0) {
			String company = list2.get(0).getCompany();
			if(company!=null) {
				LinkedList<Taxi> taxis = new LinkedList<Taxi>();
				for(int i = 0; i < list2.size(); i++) {
					String company1 = list2.get(i).getCompany();
					if(company1 == null) {
						Compania companyToAdd = new Compania(company,taxis);
						companias.add(companyToAdd);
						break;
					}
					if(company.equals(company1)) {
						taxis.add(list2.get(i));
					}
					else {
						Compania companyToAdd = new Compania(company,taxis);
						companias.add(companyToAdd);
						company = company1;
						taxis = new LinkedList<Taxi>();
						taxis.add(list2.get(i));
					}
				}
			}
		}

		return companias;
	}

	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) 
	{
		// TODO Auto-generated method stub
		Taxi mayor = null;
		try {
			LinkedList<Servicio> services = new LinkedList<Servicio>();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date initial = format.parse(rango.getFechaInicial() + " " + rango.getHoraInicio());
			Date finals = format.parse(rango.getFechaFinal() + " " + rango.getHoraFinal());

			for(int i=0; i<list1.size(); i++) {
				Servicio actual = list1.get(i);
				String company = actual.getCompany();
				Date fecha2 = actual.getEndTime();
				if((fecha2 != null && ((initial.compareTo(fecha2) == 0) || fecha2.after(initial))) && (((finals.compareTo(fecha2) == 0) || fecha2.before(finals))) && company != null && company.equals(nomCompania)) {
					services.add(actual);
				}

			}

			MergeSort.sort(services, new ServiceTaxiIdComparator(), true);

			String actual = services.get(0).getTaxiId();
			double totalFacturacion = 0;
			double facMayor = Integer.MIN_VALUE;
			String idMayor = "Ninguno";
			for(int i = 0; i < services.size(); i++) {
				String idComparar = services.get(i).getTaxiId();
				if(actual.equals(idComparar)) {
					totalFacturacion += services.get(i).getTripTotal();
				}
				else {
					if(totalFacturacion > facMayor) {
						facMayor = totalFacturacion;
						idMayor = actual;
					}
					totalFacturacion = services.get(i).getTripTotal();
					actual = idComparar;
				}
			}

			mayor = new Taxi();
			mayor.setCompany(nomCompania);
			mayor.setId(idMayor);

		}
		catch(ParseException e) {
			System.out.println("Hubo un problema en el parse de las Fechas");
		}
		return mayor;
	}

	@Override //3B
	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{
		// TODO Auto-generated method stub
		ServiciosValorPagado[] answer = new ServiciosValorPagado[3];

		try {

			if(list1.size()!=0) {

				LinkedList<Servicio> services = new LinkedList<Servicio>();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				Date initial = format.parse(rango.getFechaInicial() + " " + rango.getHoraInicio());
				Date finals = format.parse(rango.getFechaFinal() + " " + rango.getHoraFinal());

				for(int i=0; i<list1.size(); i++) {
					Servicio actual = list1.get(i);
					Date fecha = actual.getStartTime();
					Date fecha2 = actual.getEndTime();
					if((fecha != null && ((initial.compareTo(fecha) == 0) || fecha.after(initial))) && (fecha2 != null && ((finals.compareTo(fecha2) == 0) || fecha2.before(finals)))) {
						services.add(actual);
					}

				}

				int zona = Integer.parseInt(idZona);
				ServiciosValorPagado mismaZona = new ServiciosValorPagado();
				ServiciosValorPagado zonaIni = new ServiciosValorPagado();
				ServiciosValorPagado zonaFin = new ServiciosValorPagado();
				LinkedList<Servicio> mismaZ = new LinkedList<Servicio>();
				LinkedList<Servicio> iniZ = new LinkedList<Servicio>();
				LinkedList<Servicio> finZ = new LinkedList<Servicio>();
				double valorMisma = 0.0;
				double valorIni = 0.0;
				double valorFin = 0.0;

				for(int j=0; j<services.size();j++) {

					Servicio actual = list1.get(j);
					if(actual.getPickUpArea() == zona && actual.getDropOffArea() == zona) {
						mismaZ.add(actual);
						valorMisma += actual.getTripTotal();
					}
					else if(actual.getPickUpArea() == zona) {
						iniZ.add(actual);
						valorIni += actual.getTripTotal();
					}
					else if(actual.getDropOffArea() == zona){
						finZ.add(actual);
						valorFin += actual.getTripTotal();
					}

				}
				mismaZona.setServiciosAsociados(mismaZ);
				mismaZona.setValorAcumulado(valorMisma);
				zonaIni.setServiciosAsociados(iniZ);
				zonaIni.setValorAcumulado(valorIni);
				zonaFin.setServiciosAsociados(finZ);
				zonaFin.setValorAcumulado(valorFin);
				answer[0] = mismaZona;
				answer[1] = zonaIni;
				answer[2] = zonaFin;
			}

		}
		catch(ParseException e) {
			System.out.println("Hubo un problema en el parse de las Fechas");
		}
		return answer;
	}

	@Override //4B
	public ILinkedList<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		LinkedList<ZonaServicios> answer = new LinkedList<ZonaServicios>();

		MergeSort.sort(list1, new ServiceAreaComparator(), true);
		try {
			LinkedList<Servicio> services = new LinkedList<Servicio>();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date initial = format.parse(rango.getFechaInicial() + " " + rango.getHoraInicio());
			Date finals = format.parse(rango.getFechaFinal() + " " + rango.getHoraFinal());

			for(int i=0; i<list1.size(); i++) {

				Servicio actual = list1.get(i);
				Date fecha = actual.getStartTime();
				if((fecha != null && ((initial.compareTo(fecha) == 0) || fecha.after(initial))) && ((finals.compareTo(fecha) == 0) || fecha.before(finals))) {
					services.add(actual);
				}

			}
			if(services.size()!=0) {
				int zona = services.get(0).getPickUpArea();
				Date fecha = services.get(0).getStartTime();
				LinkedList<Servicio> listaSer = new LinkedList<Servicio>();
				LinkedList<FechaServicios> fechaServ = new LinkedList<FechaServicios>();
				int cuentaSer = 0;
				for(int i = 0; i<services.size(); i++) {
					if(zona == 0) {
						zona = services.get(i+1).getPickUpArea();
						fecha = services.get(i+1).getStartTime();
					}
					else {
						if(services.get(i).getPickUpArea() == zona) {
							if(services.get(i).getStartTime().compareTo(fecha) == 0) {
								listaSer.add(services.get(i));
								cuentaSer++;
							}
							else {
								FechaServicios fechaServicios = new FechaServicios();
								fechaServicios.setFecha(format.format(fecha));
								fechaServicios.setNumServicios(cuentaSer);
								fechaServicios.setServiciosAsociados(listaSer);
								fechaServ.add(fechaServicios);

								fecha = services.get(i).getStartTime();
								listaSer = new LinkedList<Servicio>();
								listaSer.add(services.get(i));
								cuentaSer = 1;
							}
						}
						else {
							FechaServicios fechaServicios2 = new FechaServicios();
							fechaServicios2.setFecha(format.format(fecha));
							fechaServicios2.setNumServicios(cuentaSer);
							fechaServicios2.setServiciosAsociados(listaSer);
							fechaServ.add(fechaServicios2);
							ZonaServicios zonaServicios = new ZonaServicios();
							zonaServicios.setIdZona(""+zona);
							zonaServicios.setFechasServicios(fechaServ);
							answer.add(zonaServicios);
							fechaServ =  new LinkedList<FechaServicios>();
							zona=services.get(i).getPickUpArea();
							listaSer = new LinkedList<Servicio>();
							listaSer.add(services.get(i));
							cuentaSer = 1;
							fecha = services.get(i).getStartTime();
						}

					}
				}
			}
		}
		catch(ParseException e) {
			System.out.println("Hubo un problema en el parse de las Fechas");
		}

		return answer;
	}

	@Override //2C
	public ILinkedList<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n)
	{
		// TODO Auto-generated method stub
		LinkedList<CompaniaServicios> companias = new LinkedList<CompaniaServicios>();
		try {
			LinkedList<Servicio> services = new LinkedList<Servicio>();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date initial = format.parse(rango.getFechaInicial() + " " + rango.getHoraInicio());
			Date finals = format.parse(rango.getFechaFinal() + " " + rango.getHoraFinal());

			for(int i=0; i<list1.size(); i++) {

				Servicio actual = list1.get(i);
				Date fecha = actual.getStartTime();
				if((fecha != null && ((initial.compareTo(fecha) == 0) || fecha.after(initial))) && ((finals.compareTo(fecha) == 0) || fecha.before(finals))) {
					services.add(actual);
				}

			}

			MergeSort.sort(services, new ServiceCompanyComparator(), true);

			String company = services.get(0).getCompany();
			if(company!=null) {
				LinkedList<Servicio> servicios = new LinkedList<Servicio>();
				for(int i = 0; i < services.size(); i++) {
					String company1 = services.get(i).getCompany();
					if(company1 == null) {
						CompaniaServicios companyToAdd = new CompaniaServicios();
						companyToAdd.setNomCompania(company);
						companyToAdd.setServicios(servicios);
						companias.add(companyToAdd);
						break;
					}
					else {
						if(company.equals(company1)) {
							servicios.add(services.get(i));
						}
						else {
							CompaniaServicios companyToAdd = new CompaniaServicios();
							companyToAdd.setNomCompania(company);
							companyToAdd.setServicios(servicios);
							companias.add(companyToAdd);
							company = company1;
							servicios = new LinkedList<Servicio>();
							servicios.add(services.get(i));
						}
					}
				}


			}
		}
		catch(ParseException e) {
			System.out.println("Hubo un problema en el parse de las Fechas");
		}
		if(companias.size()!=0) {
			MergeSort.sort(companias, new CompanyServicesComparator(), false);
		}

		return companias;
	}

	@Override //3C
	public ILinkedList<CompaniaTaxi> taxisMasRentables()
	{
		// TODO Auto-generated method stub
		LinkedList<CompaniaTaxi> companiasTaxi = new LinkedList<CompaniaTaxi>();
		MergeSort.sort(list1, new ServiceCompanyTaxiIdComparator(), true);

		String company = list1.get(0).getCompany();
		if(company != null) {
			String idActual = list1.get(0).getTaxiId();
			String elMasRentable = "Ninguno";
			double mayorRentabilidad = Integer.MIN_VALUE;
			double ganancias = 0.0;
			double distance = 0.0;
			for(int i = 0; i<list1.size(); i++) {
				String company1 = list1.get(i).getCompany();
				if(company1 == null) {
					CompaniaTaxi compania = new CompaniaTaxi();
					compania.setNomCompania(company);
					Taxi taxi = new Taxi();
					taxi.setCompany(company);
					taxi.setId(elMasRentable);
					compania.setTaxi(taxi);
					companiasTaxi.add(compania);
					break;
				}
				else {
					if(company.equals(company1)) {
						if(idActual.equals(list1.get(i).getTaxiId())) {
							ganancias += list1.get(i).getTripTotal();
							distance += list1.get(i).getTripMiles();
						}
						else {
							double rentabilidad = distance != 0 ? ganancias/distance : ganancias;
							if(rentabilidad > mayorRentabilidad) {
								mayorRentabilidad = rentabilidad;
								elMasRentable = list1.get(i-1).getTaxiId();
							}
							idActual = list1.get(i).getTaxiId();
							ganancias = list1.get(i).getTripTotal();
							distance = list1.get(i).getTripMiles();
						}
					}
					else {
						double rentabilidad = distance != 0 ? ganancias/distance : ganancias;
						if(rentabilidad > mayorRentabilidad) {
							mayorRentabilidad = rentabilidad;
							elMasRentable = list1.get(i-1).getTaxiId();
						}
						CompaniaTaxi compania = new CompaniaTaxi();
						compania.setNomCompania(company);
						Taxi taxi = new Taxi();
						taxi.setCompany(company);
						taxi.setId(elMasRentable);
						compania.setTaxi(taxi);
						companiasTaxi.add(compania);
						company = company1;
						idActual = list1.get(i).getTaxiId();
						ganancias = list1.get(i).getTripTotal();
						distance = list1.get(i).getTripMiles();
						mayorRentabilidad = Integer.MIN_VALUE;
					}
				}
			}
		}
		return companiasTaxi;
	}

	@Override //4C
	public IStack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		// TODO Auto-generated method stub
		Stack<Servicio> servicios = new Stack<Servicio>();
		Stack<Servicio> servicios1 = new Stack<Servicio>();
		try {
			LinkedList<Servicio> services = new LinkedList<Servicio>();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date initial = format.parse(fecha + " " + horaInicial);
			Date finals = format.parse(fecha + " " + horaFinal);

			for(int i=0; i<list1.size(); i++) {

				Servicio actual = list1.get(i);
				Date fecha1 = actual.getStartTime();
				Date fecha2 = actual.getEndTime();
				if((fecha1 != null && ((initial.compareTo(fecha1) == 0) || fecha1.after(initial))) && ((finals.compareTo(fecha2) == 0) || fecha2.before(finals)) && actual.getTaxiId().equals(taxiId)) {
					services.add(actual);
				}

			}
			MergeSort.sort(services, new ServiceDateComparator(), true);
			//			Node<Servicio> actualll = services.getNode(0);
			//			while(actualll != null) {
			//				System.out.println(format.format(actualll.getElement().getStartTime()) + "   " + format.format(actualll.getElement().getEndTime()));
			//				actualll = actualll.getNext();
			//			}
			double distTotal = 0.0;
			int cantResumenes = 0;
			for(int i = 0; i< services.size(); i++) {
				if(services.get(i).getTripMiles() + distTotal >= 20) {
					Date fechaFinal = null;
					Date fechaInicial = null;
					double tripTotal = 0.0;
					double tripSeconds = 0.0;
					int tamano = servicios.size();
					while(servicios.size() > cantResumenes) {
						Servicio actual = servicios.pop();
						if(servicios.size() == tamano-1) {

							fechaFinal = actual.getEndTime();
						}
						if(servicios.size()== cantResumenes) {
							fechaInicial = actual.getStartTime();
						}
						tripTotal += actual.getTripTotal();
						tripSeconds += actual.getTripSeconds();
					}
					Servicio nuevo = new Servicio();
					cantResumenes++;
					nuevo.inicializarServicio("Servicio Resumen" + (cantResumenes), "", "", tripSeconds, distTotal, tripTotal, fechaInicial, fechaFinal, -1, -1);
					servicios.push(nuevo);
					servicios.push(services.get(i));
					distTotal = services.get(i).getTripMiles();
				}
				else {
					servicios.push(services.get(i));
					distTotal += services.get(i).getTripMiles();
				}
			}
		}
		catch(ParseException e) {
			System.out.println("Hubo un problema en el parse de las Fechas");
		}
		while(!servicios.isEmpty()) {
			servicios1.push(servicios.pop());
		}
		return servicios1;
	}

}
