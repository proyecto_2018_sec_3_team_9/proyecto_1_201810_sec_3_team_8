package model.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import model.data_structures.ILinkedList;

public class FechaServicios implements Comparable<FechaServicios>{
	
	private String fecha;
	private ILinkedList<Servicio> serviciosAsociados;
	private int numServicios;
	
	
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public ILinkedList<Servicio> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void setServiciosAsociados(ILinkedList<Servicio> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public int getNumServicios() {
		return numServicios;
	}
	public void setNumServicios(int numServicios) {
		this.numServicios = numServicios;
	}
	@Override
	public int compareTo(FechaServicios o) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		int compare = -1;
		try {
		if(format.parse(fecha).compareTo(format.parse(o.getFecha())) == 0){
			compare = 0;
		}
		else if(format.parse(fecha).compareTo(format.parse(o.getFecha())) > 0) {
			compare= 1;
		}
		}
		catch(ParseException e) {
			System.out.println("Hubo un problema en el parse de las Fechas");
		}
		return compare;
	}
	
	

}
