package model.vo;

import model.data_structures.ILinkedList;
import model.data_structures.LinkedList;


public class Compania implements Comparable<Compania> {
	
	private String nombre;
	
	private ILinkedList<Taxi> taxisInscritos;	

	public Compania(String pNombre, LinkedList<Taxi> pTaxi) {
		nombre = pNombre;
		taxisInscritos = pTaxi;
	}

	public String getNombre() 
	{
		return nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public ILinkedList<Taxi> getTaxisInscritos() 
	{
		return taxisInscritos;
	}

	public void setTaxisInscritos(ILinkedList<Taxi> taxisInscritos) 
	{
		this.taxisInscritos = taxisInscritos;
	}

	@Override
	public int compareTo(Compania o) {
		// TODO Auto-generated method stub
		int compare = -1;
		if(nombre.equals(o.getNombre())){
			compare = 0;
		}
		else if(nombre.compareTo(o.getNombre()) > 0) {
			return 1;
		}
		return compare;
	}
	

}
